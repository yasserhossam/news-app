package com.example.shaimaaderbaz.thenewsapp.views.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.shaimaaderbaz.thenewsapp.R;
import com.example.shaimaaderbaz.thenewsapp.data.model.Article;
import com.example.shaimaaderbaz.thenewsapp.data.repository.RemoteNewsRepositry;
import com.example.shaimaaderbaz.thenewsapp.presenters.MainPresenterImp;
import com.example.shaimaaderbaz.thenewsapp.views.adapter.NewsAdapter;
import com.example.shaimaaderbaz.thenewsapp.views.view.NewsView;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity implements NewsView,
        NewsAdapter.NewsItemListener {

    @BindView(R.id.recycler_news)
    RecyclerView newsRecyclerView;

    private MainPresenterImp mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenter = new MainPresenterImp(this, new RemoteNewsRepositry(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.getAllNews();
    }

    @Override
    public void showArticles(List<Article> articles) {
        newsRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        NewsAdapter newsAdapter = new NewsAdapter(articles, this);
        newsRecyclerView.setAdapter(newsAdapter);
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onItemClicked(Article article) {

    }
}
