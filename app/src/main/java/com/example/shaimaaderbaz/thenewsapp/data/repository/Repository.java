package com.example.shaimaaderbaz.thenewsapp.data.repository;

import java.util.List;

/**
 * Created by Shaimaa Derbaz on 4/9/2019.
 */

public interface Repository<T> {
    List<T> getAllNews();
   // void setTasksDataToDatabase(List<T> data);
 //  void setNewsData(T data);
}
