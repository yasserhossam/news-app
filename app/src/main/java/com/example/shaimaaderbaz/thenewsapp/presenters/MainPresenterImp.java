package com.example.shaimaaderbaz.thenewsapp.presenters;

import com.example.shaimaaderbaz.thenewsapp.data.model.Article;
import com.example.shaimaaderbaz.thenewsapp.data.repository.Repository;
import com.example.shaimaaderbaz.thenewsapp.views.view.NewsView;

import java.util.List;


/**
 * Created by Shaimaa Derbaz on 4/22/2019.
 */

public class MainPresenterImp implements MainPresenter<Article> {
    private NewsView mView;

    private Repository msRepository;

    public MainPresenterImp(NewsView newsView, Repository spr) {
        mView = newsView;
        msRepository = spr;
    }

    @Override
    public List<Article> getAllNews()
    {

        List<Article> articles =msRepository.getAllNews();
        return articles;
    }
}
